﻿using System;

namespace CrayonAPI.Util
{
    public class RequestParamValidator
    {
        public Func<bool> Rule { get; }

        public string Message { get; }

        public RequestParamValidator(Func<bool> rule, string message)
        {
            Rule = rule;
            Message = message;
        }

        public static RequestParamValidator ExchangeIdValidator(int id) =>
            new RequestParamValidator(
                IdInvalidRule(id), "Exchange id is not valid.");

        private static Func<bool> IdInvalidRule(int id)
        {
            return () => id <= 0;
        }
    }
}
