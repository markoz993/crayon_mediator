﻿using CrayonAPI.Logging;
using CrayonAPI.Util;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;
using System.Linq;
using System;

using MediatR;

namespace CrayonAPI.Controllers
{
    public class BaseApiController : Controller
    {
        private IMediator _mediator;
        protected IMediator Mediator
        {
            get
            {
                return _mediator ?? (_mediator = HttpContext.RequestServices.GetService<IMediator>());
            }
        }

        private ILogger _logger;
        protected ILogger Logger
        {
            get
            {
                return _logger ?? (
                    _logger = Logging.Logger.Create(@"D:\logs.txt"));
            }
        }

        public BaseApiController(IMediator mediator)
        {
            _mediator = mediator;
        }

        protected async Task<IActionResult> ProcessRequest<T>(Func<Task<T>> request, params RequestParamValidator[] paramValidators)
        {
            var invalidParams = paramValidators?
                .Where(pv => pv.Rule());

            if (invalidParams.Any())
            {
                var message = string.Join(
                    ". ", invalidParams.Select(s => s.Message));
                return BadRequest(message);
            }

            try
            {
                var response = await request();

                if (response == null)
                {
                    return NotFound();
                }

                return View(response);
            }

            catch (Exception e)
            {
                Logger.LogMessage(e.ToString());
                return StatusCode(500);
            }
        }
    }
}