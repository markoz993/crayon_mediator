﻿using Crayon.Application.Commands;
using Crayon.Service.ViewModels;

using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

using MediatR;

namespace CrayonAPI.Controllers
{
    public class HomeController : BaseApiController
    {
        public HomeController(IMediator mediator) : base(mediator) { }

        public async Task<IActionResult> Index() =>
            await ProcessRequest(request: () => Mediator.Send(new GetExchangeRateStartDataRequest()));

        [HttpPost]
        public async Task<IActionResult> Index(GetExchangeRatesViewModel model) =>
            await ProcessRequest(request: () => Mediator.Send(new GetExchangeRateRequest
            { BaseCurrency = model.Base, TargetCurrency = model.Target, Dates = model.Dates }));
    }
}