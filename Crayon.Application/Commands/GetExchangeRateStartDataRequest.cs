﻿using Crayon.Service.ViewModels;

using MediatR;

namespace Crayon.Application.Commands
{
    public class GetExchangeRateStartDataRequest : IRequest<GetExchangeRatesViewModel>
    {
    }
}
