﻿using Crayon.Service.ViewModels;

using MediatR;

namespace Crayon.Application.Commands
{
    public class GetExchangeRateRequest : IRequest<GetExchangeRatesViewModel>
    {
        public string BaseCurrency { get; set; }
        public string TargetCurrency { get; set; }
        public string[] Dates { get; set; }
    }
}
