﻿using Crayon.Service.Abstract;
using Crayon.Service.ViewModels;

using System.Threading.Tasks;
using System.Threading;

using MediatR;

namespace Crayon.Application.Commands
{
    public class GetExchangeRateStartDataHandler : IRequestHandler<GetExchangeRateStartDataRequest, GetExchangeRatesViewModel>
    {
        private readonly IExchangeService _exchangeService;

        public GetExchangeRateStartDataHandler(IExchangeService exchangeS)
        {
            _exchangeService = exchangeS;
        }

        public async Task<GetExchangeRatesViewModel> Handle(GetExchangeRateStartDataRequest request, CancellationToken cancellationToken) =>
            await Task.Run(() => _exchangeService.GetInitialExchangeRateData());
    }
}
