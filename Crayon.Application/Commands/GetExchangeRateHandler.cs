﻿using Crayon.Service.ViewModels;
using Crayon.Service.Abstract;

using System.Threading.Tasks;
using System.Threading;

using MediatR;

namespace Crayon.Application.Commands
{
    class GetExchangeRateHandler : IRequestHandler<GetExchangeRateRequest, GetExchangeRatesViewModel>
    {
        private readonly IExchangeService _exchangeService;

        public GetExchangeRateHandler(IExchangeService exchangeS)
        {
            _exchangeService = exchangeS;
        }

        public async Task<GetExchangeRatesViewModel> Handle(GetExchangeRateRequest request, CancellationToken cancellationToken) =>
            await Task.Run(() => _exchangeService.GetExchangeRates(request.BaseCurrency, request.TargetCurrency, request.Dates));
    }
}
