﻿using Crayon.Domain.Entities;
using Crayon.Domain.Models;

using AutoMapper;

namespace Crayon.Repository.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            this.CreateMap<Exchange, ExchangeDTO>().ReverseMap();
        }
    }
}
