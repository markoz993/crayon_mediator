﻿namespace Crayon.Repository.Util
{
    public static class CurrencyList
    {
        public static string[] GetArrayOfCurrencies() =>
            new string[]
            {
                "CAD",
                "HKD",
                "ISK",
                "PHP",
                "DKK",
                "HUF",
                "CZK",
                "AUD",
                "RON",
                "SEK",
                "IDR",
                "INR",
                "BRL",
                "RUB",
                "HRK",
                "JPY",
                "THB",
                "CHF",
                "SGD",
                "PLN",
                "BGN",
                "TRY",
                "CNY",
                "NOK",
                "NZD",
                "ZAR",
                "USD",
                "MXN",
                "ILS",
                "GBP",
                "KRW",
                "MYR"
            };
    }
}
