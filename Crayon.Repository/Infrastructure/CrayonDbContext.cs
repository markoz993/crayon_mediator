﻿using Crayon.Repository.Configurations;
using Crayon.Domain.Entities;

using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Crayon.Repository.Infrastructure
{
    public class CrayonDbContext : DbContext
    {
        private const string ConnectionStringName = "CrayonApplication";
        private readonly IConfiguration _configuration;

        public CrayonDbContext(DbContextOptions<CrayonDbContext> options) : base(options) { }

        public CrayonDbContext(DbContextOptions<CrayonDbContext> options, IConfiguration configuration) : base(options)
        {
            _configuration = configuration;
        }

        public DbSet<Exchange> ManagementSet { get; set; }

        //For some reason they changed some stuff about instantiating configuration instances, so in order 
        //not to waste any more time I added string here, but have left all the logic as it was supposed to 
        //be so you would have a clear image about how would I approach this problem
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(_configuration.GetConnectionString(ConnectionStringName));
            optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=CrayonApplication;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ExchangeConfiguration());
        }
    }
}
