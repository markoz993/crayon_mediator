﻿namespace Crayon.Repository.Infrastructure
{
    public interface IDbContextFactory
    {
        CrayonDbContext DbContext { get; }
    }
}
