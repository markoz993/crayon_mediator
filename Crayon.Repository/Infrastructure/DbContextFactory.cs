﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

using System;

namespace Crayon.Repository.Infrastructure
{
    public class DbContextFactory : IDesignTimeDbContextFactory<CrayonDbContext>, IDbContextFactory, IDisposable
    {
        public CrayonDbContext DbContext { get; private set; }

        public DbContextFactory() { }

        public DbContextFactory(IOptions<DbContextSettings> settings)
        {
            var options = new DbContextOptionsBuilder().UseSqlServer(settings.Value.ConnectionString).Options as DbContextOptions<CrayonDbContext>;
            DbContext = new CrayonDbContext(options, GetConfiguration());
        }

        public CrayonDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<CrayonDbContext>();
            return new CrayonDbContext(builder.Options, GetConfiguration());
        }

        //For some reason they changed some stuff about this part, so I left it this way just in order for you to 
        //know how was I doing it before, but now, there is no support for given methods. Now it can't find the 
        //appsettings.json file and connection string is not available that way. I stopped working on figuring 
        //this out only because of the time available to complete this task.
        private IConfigurationRoot GetConfiguration()
        {
            string basePath = System.AppContext.BaseDirectory;
            IConfigurationRoot configuration = new ConfigurationBuilder()
             //.SetBasePath(basePath)
             //.AddJsonFile("appsettings.json")
             .Build();

            return configuration;
        }

        ~DbContextFactory()
        {
            Dispose();
        }
        public void Dispose()
        {
            DbContext?.Dispose();
        }
    }
}
