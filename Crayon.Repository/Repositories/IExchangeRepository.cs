﻿using Crayon.Domain.Entities;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Crayon.Repository.Repositories
{
    public interface IExchangeRepository
    {
        Task<IEnumerable<Exchange>> GetAllAsync();
        Task<Exchange> GetByIDAsync(int exchangeID);

        Task AddAsync(Exchange exchange);
        Task EditAsync(Exchange exchange);
        Task DeleteAsync(Exchange exchange);
    }
}
