﻿using Crayon.Repository.Infrastructure;
using Crayon.Domain.Entities;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Crayon.Repository.Repositories
{
    public class ExchangeRepository : EntityBaseRepository<Exchange>, IExchangeRepository
    {
        public ExchangeRepository(IDbContextFactory factory) : base(factory) { }

        public async Task<IEnumerable<Exchange>> GetAllAsync() =>
            await GetAll().ToListAsync();


        public async Task<Exchange> GetByIDAsync(int exchangeID) =>
            await FindByIncluding(p => p.ID == exchangeID).FirstOrDefaultAsync();


        public async Task AddAsync(Exchange exchange)
        {
            Add(exchange);
            await SaveAsync();
        }
        public async Task EditAsync(Exchange exchange)
        {
            Edit(exchange);
            await SaveAsync();
        }
        public async Task DeleteAsync(Exchange exchange)
        {
            Delete(exchange);
            await SaveAsync();
        }
    }
}
