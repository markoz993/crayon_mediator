﻿using Crayon.Repository.Infrastructure;

using Microsoft.Extensions.DependencyInjection;

namespace Crayon.Repository
{
    public static class RepositoryInjectionModule
    {
        /// <summary>
        ///  Dependency inject DbContextFactory and concrete repository
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection InjectPersistence(this IServiceCollection services)
        {
            services.AddScoped<IDbContextFactory, DbContextFactory>();
            return services;
        }
    }
}
