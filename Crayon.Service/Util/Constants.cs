﻿namespace Crayon.Service.Util
{
    public static class Constants
    {
        public const string EXCHANGE_RATES_URL = "https://api.exchangeratesapi.io/";
        public const string LATEST_EXCHANGE_RATES_URL = "https://api.exchangeratesapi.io/latest";
        public const string HISTORY_EXCHANGE_RATES_URL = "https://api.exchangeratesapi.io/history";
    }
}
