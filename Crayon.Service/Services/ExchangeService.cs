﻿using Crayon.Service.ExternalApi;
using Crayon.Service.ViewModels;
using Crayon.Service.Abstract;
using Crayon.Repository.Util;
using Crayon.Service.Models;

using System.Collections.Generic;
using System.Linq;
using System;

namespace Crayon.Service.Services
{
    public class ExchangeService : IExchangeService
    {
        private readonly IExchangeApiService _exchangeApi;

        public ExchangeService(IExchangeApiService exchangeApi)
        {
            _exchangeApi = exchangeApi;
        }

        public GetExchangeRatesViewModel GetInitialExchangeRateData() =>
            new GetExchangeRatesViewModel
            {
                CurrencyList = CurrencyList.GetArrayOfCurrencies(),                
            };

        public GetExchangeRatesViewModel GetExchangeRates(string baseCurrency, string targetCurrency, string[] dates)
        {
            List<DateTime> dateObjects = dates.Select(d => Convert.ToDateTime(d)).ToList();
            List<HistoryRate> rates = GetRatesFromAPI(baseCurrency, targetCurrency, dateObjects);

            return FillModelData(baseCurrency, targetCurrency, dates, rates);
        }

        private List<HistoryRate> GetRatesFromAPI(string baseCurrency, string targetCurrency, List<DateTime> dates)
        {
            List<HistoryRate> result = new List<HistoryRate>();
            if (dates.Any())
            {
                foreach(var date in dates)
                    result.Add(_exchangeApi.GetData(baseCurrency, targetCurrency, date));
            }
            return result;
        }

        private GetExchangeRatesViewModel FillModelData(string baseCurrency, string targetCurrency, string[] dates, List<HistoryRate> rates) =>
            new GetExchangeRatesViewModel
            {
                CurrencyList = CurrencyList.GetArrayOfCurrencies(),
                Base = baseCurrency,
                Target = targetCurrency,
                Dates = dates,
                AverageRate = CalculateAverageRate(rates, targetCurrency),
                MaxRate = GetMaxRate(rates, targetCurrency),
                MaxRateDate = GetMaxRateDate(rates, targetCurrency),
                MinRate = GetMinRate(rates, targetCurrency),
                MinRateDate = GetMinRateDate(rates, targetCurrency)
            };

        private decimal CalculateAverageRate(List<HistoryRate> historyRates, string target)
        {
            int counter = 0;
            decimal sum = 0;
            foreach (var rate in historyRates)
            {
                var firstRate = rate.Rates.FirstOrDefault();
                firstRate.TryGetValue(target, out List<Currency> currencies);
                if (currencies.Any())
                {
                    var currency = currencies.FirstOrDefault();
                    counter++;
                    sum += currency.Value;
                }
            }
            return sum / counter;
        }

        private decimal GetMaxRate(List<HistoryRate> historyRates, string target)
        {
            decimal max = 0;
            foreach (var rate in historyRates)
            {
                var firstRate = rate.Rates.FirstOrDefault();
                firstRate.TryGetValue(target, out List<Currency> currencies);
                if (currencies.Any())
                {
                    var currency = currencies.FirstOrDefault();
                    if (currency.Value > max)
                        max = currency.Value;
                }
            }
            return max;
        }

        private DateTime GetMaxRateDate(List<HistoryRate> historyRates, string target)
        {
            decimal max = 0;
            DateTime result = new DateTime(1900, 1, 1);
            foreach (var rate in historyRates)
            {
                var firstRate = rate.Rates.FirstOrDefault();
                firstRate.TryGetValue(target, out List<Currency> currencies);
                if (currencies.Any())
                {
                    var currency = currencies.FirstOrDefault();
                    if (currency.Value > max)
                    {
                        max = currency.Value;
                        if (DateTime.TryParse(firstRate.Keys.FirstOrDefault(), out DateTime newResult))
                            result = newResult;
                    }
                }
            }
            return result;
        }

        private decimal GetMinRate(List<HistoryRate> historyRates, string target)
        {
            decimal min = decimal.MaxValue;
            foreach (var rate in historyRates)
            {
                var firstRate = rate.Rates.FirstOrDefault();
                firstRate.TryGetValue(target, out List<Currency> currencies);
                if (currencies.Any())
                {
                    var currency = currencies.FirstOrDefault();
                    if (currency.Value < min)
                        min = currency.Value;
                }
            }
            return min;
        }

        private DateTime GetMinRateDate(List<HistoryRate> historyRates, string target)
        {
            decimal min = decimal.MaxValue;
            DateTime result = new DateTime(1900, 1, 1);
            foreach (var rate in historyRates)
            {
                var firstRate = rate.Rates.FirstOrDefault();
                firstRate.TryGetValue(target, out List<Currency> currencies);
                if (currencies.Any())
                {
                    var currency = currencies.FirstOrDefault();
                    if (currency.Value < min)
                    {
                        min = currency.Value;
                        if (DateTime.TryParse(firstRate.Keys.FirstOrDefault(), out DateTime newResult))
                            result = newResult;
                    }
                }
            }
            return result;
        }
    }
}
