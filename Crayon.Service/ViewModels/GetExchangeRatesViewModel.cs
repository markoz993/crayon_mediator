﻿using System;

namespace Crayon.Service.ViewModels
{
    public class GetExchangeRatesViewModel
    {
        public string[] CurrencyList { get; set; }
        public string[] Dates { get; set; }
        public string Base { get; set; }
        public string Target { get; set; }

        public decimal MaxRate { get; set; }
        public decimal MinRate { get; set; }
        public decimal AverageRate { get; set; }
        public DateTime MaxRateDate { get; set; }
        public DateTime MinRateDate { get; set; }
    }
}
