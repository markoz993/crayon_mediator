﻿using Crayon.Service.Extensions;

using Newtonsoft.Json;

using System.Collections.Generic;
using System;

namespace Crayon.Service.Models
{
    public class HistoryRate
    {
        [JsonProperty(PropertyName = "base")]
        public string Base { get; set; }

        [JsonProperty(PropertyName = "rates")]
        public List<Dictionary<string, List<Currency>>> Rates { get; set; }

        [JsonProperty(PropertyName = "start_at")]
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime StartAt { get; set; }

        [JsonProperty(PropertyName = "end_at")]
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime EndAT { get; set; }
    }
}
