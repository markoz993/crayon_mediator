﻿namespace Crayon.Service.Models
{
    public class Currency
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
    }
}
