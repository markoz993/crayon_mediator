﻿using System.Collections.Generic;
using System;

using Newtonsoft.Json;

namespace Crayon.Service.Models
{
    public class LatestRate
    {
        [JsonProperty(PropertyName ="base")]
        public string Base { get; set; }

        [JsonProperty(PropertyName ="target")]
        public string Target { get; set; }

        [JsonProperty(PropertyName ="rates")]
        public List<Currency> Rates { get; set; }

        [JsonProperty(PropertyName ="date")]
        public DateTime Date { get; set; }
    }    
}
