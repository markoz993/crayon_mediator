﻿using Crayon.Service.ViewModels;

namespace Crayon.Service.Abstract
{
    public interface IExchangeService
    {
        GetExchangeRatesViewModel GetInitialExchangeRateData();

        GetExchangeRatesViewModel GetExchangeRates(string baseCurrency, string targetCurrency, string[] dates);
    }
}
