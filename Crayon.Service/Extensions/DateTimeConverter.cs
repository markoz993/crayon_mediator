﻿using Newtonsoft.Json.Converters;

namespace Crayon.Service.Extensions
{
    public class DateTimeConverter : IsoDateTimeConverter
    {
        public DateTimeConverter()
        {
            base.DateTimeFormat = "yyyy-MM-dd";
        }
    }
}
