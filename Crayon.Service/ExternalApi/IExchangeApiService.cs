﻿using Crayon.Service.Models;
using Crayon.Service.ViewModels;

using System;

namespace Crayon.Service.ExternalApi
{
    public interface IExchangeApiService
    {
        HistoryRate GetData(string baseCurrency, string targetCurrency, DateTime date);
    }
}
