﻿using Crayon.Service.Models;
using Crayon.Service.Util;

using Polly.Retry;
using Polly;

using Newtonsoft.Json;

using System.Net.Http;
using System.Net;
using System;

namespace Crayon.Service.ExternalApi
{
    public class ExchangeApiService : IExchangeApiService
    {
        readonly AsyncRetryPolicy<HttpResponseMessage> _httpRequestPolicy;

        public ExchangeApiService()
        {
            _httpRequestPolicy = Policy.HandleResult<HttpResponseMessage>
               (r => r.StatusCode == HttpStatusCode.InternalServerError)
               .WaitAndRetryAsync(3, retryAttempt => TimeSpan.FromSeconds(retryAttempt));
        }

        public HistoryRate GetData(string baseCurrency, string targetCurrency, DateTime date)
        {
            var httpClient = new HttpClient();            
            httpClient.DefaultRequestHeaders.Add("start_at", FormatDateToString(date));
            httpClient.DefaultRequestHeaders.Add("end_at", FormatDateToString(date));
            httpClient.DefaultRequestHeaders.Add("base", baseCurrency);
            httpClient.DefaultRequestHeaders.Add("symbols", targetCurrency);
            HttpResponseMessage httpResponse = _httpRequestPolicy.ExecuteAsync(() => httpClient.GetAsync(Constants.HISTORY_EXCHANGE_RATES_URL)).Result;
            if (httpResponse.IsSuccessStatusCode)
            {
                var responseBody = httpResponse.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<HistoryRate>(responseBody);
            }
            else
            {
                HandleNotificationSenderStatus(httpResponse.StatusCode);
            }
            return new HistoryRate();
        }

        private string FormatDateToString(DateTime date) =>
            date.ToString("yyyy-mm-dd");
        
        private void HandleNotificationSenderStatus(HttpStatusCode status)
        {
            switch (status)
            {
                case HttpStatusCode.Forbidden:
                    throw new Exception("Code 403 was thrown.");
                case HttpStatusCode.NotFound:
                    throw new Exception("Code 404 was thrown.");
                case HttpStatusCode.PreconditionFailed:
                    throw new Exception("Code 412 was thrown.");
                case HttpStatusCode.BadGateway:
                    throw new Exception("Code 502 was thrown.");
                default:
                    throw new Exception($"Code {status} was thrown.");
            }
        }
    }
}
