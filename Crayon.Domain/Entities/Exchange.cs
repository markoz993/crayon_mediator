﻿using System;

namespace Crayon.Domain.Entities
{
    public class Exchange : IEntityBase
    {
        public int ID { get; set; }

        public DateTime ExchangeDate { get; set; }

        public string BaseCurrency { get; set; }
        public string TargetCurrency { get; set; }

        public decimal ExchangeRate { get; set; }
    }
}
