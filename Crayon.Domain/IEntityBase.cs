﻿namespace Crayon.Domain
{
    public interface IEntityBase
    {
        int ID { get; set; }
    }
}
