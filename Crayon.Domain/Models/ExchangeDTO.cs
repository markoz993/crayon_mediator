﻿using System;

namespace Crayon.Domain.Models
{
    public class ExchangeDTO
    {
        public int ID { get; set; }

        public DateTime ExchangeDate { get; set; }

        public string BaseCurrency { get; set; }
        public string TargetCurrency { get; set; }

        public decimal ExchangeRate { get; set; }
    }
}
